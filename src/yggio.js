const config = require('./config');
const testNode = require('./test_node');
const stormApi = require('./yggio_api');

function init () {
  return Promise.resolve()
    .then(() => {
      return stormApi.registerOrLogin();
    }).then(() => {
      return stormApi.createChannel(config.yggio.channel);
    }).then((channel) => {
      var provider = config.yggio.provider;
      provider.channel = channel._id;
      return stormApi.createProvider(provider);
    }).then((provider) => {
      config.yggio.provider.secret = provider.secret;
      config.yggio.provider.client_id = provider.client_id;
      return setUpIotnode(testNode);
    });
}

function setUpIotnode (node) {
  console.log(node);
  return stormApi.createIotnode(node)
  .then((iotnode) => {
    return stormApi.createChannel({
      address: config.yggio.nodesChannelUrl,
      iotnode: iotnode._id
    });
  }, (err) => {
    if (err.error === 'duplicate secret') {
      return Promise.resolve('Node is already registered.');
    }
  });
}

module.exports = {
  init: init,
  setUpIotnode: setUpIotnode
};
