module.exports = {
  name: 'test-node',
  'node-type': 'lora-node',
  devEui: 'AAAAAAAAAAAAAAAA',
  appEui: 'AAAAAAAAAAAAAAAA',
  appKey: 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA',
  deviceModelName: 'LoPy',
  latlng: [
    56.7,
    17.2
  ],
  info: 'Short description of device'
};
