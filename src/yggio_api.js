'use strict';

const request = require('request-promise');
const config = require('./config');

let baseRequest = request.defaults({
  baseUrl: config.yggio.restApiUrl,
  json: true
});
let token = '';

const requestObject = (path, params) => {
  return {
    auth: {
      bearer: token
    },
    headers: {},
    qs: params,
    uri: path
  };
};
const requestObjectBody = (path, params, data) => {
  const reqObj = requestObject(path, params);
  reqObj.body = data;
  return reqObj;
};

function setToken (accessToken) {
  token = accessToken;
  baseRequest = baseRequest.defaults({
    auth: {
      bearer: token
    }
  });
}

function login () {
  return Promise.resolve()
    .then(() => {
      let loginRequest = baseRequest.defaults({
        baseUrl: config.yggio.yggioUrl,
        uri: '/auth/local'
      });
      return loginRequest.post({
        body: {
          username: config.yggio.username,
          password: config.yggio.password
        }
      });
    })
    .then((response) => {
      setToken(response.token);
      return Promise.resolve(response);
    });
}

function register () {
  return Promise.resolve()
    .then(() => {
      const params = {
        uri: '/users',
        body: {
          username: config.yggio.username,
          password: config.yggio.password
        }
      };
      return baseRequest.post(params);
    })
    .then((response) => {
      setToken(response.token);
      return Promise.resolve(response);
    });
}

module.exports = {
  get: (path, params) => {
    return baseRequest.get(requestObject(path, params));
  },
  delete: (path, params) => {
    return baseRequest.delete(requestObject(path, params));
  },
  put: (path, params, data) => {
    return baseRequest.put(requestObjectBody(path, params, data));
  },
  post: (path, params, data) => {
    return baseRequest.post(requestObjectBody(path, params, data));
  },

  registerOrLogin: () => {
    return register().then(function (response) {
      return Promise.resolve(response);
    }, function (err) {
      if (err.statusCode === 422) {
        return login();
      } else {
        return Promise.reject(err);
      }
    });
  },

  createIotnode: (iotnode) => {
    return baseRequest.post('/iotnodes', {
      body: iotnode
    });
  },
  getIotNodes: () => {
    return baseRequest.get('/iotnodes');
  },
  getIotNode: (id) => {
    return baseRequest.get('/iotnodes/' + id);
  },
  deleteIotNode: (id) => {
    return baseRequest.del('/iotnodes/' + id);
  },

  setStatus: (iotnodeId, status) => {
    console.log('setStatus iotnode(', iotnodeId, ') to ', status);
    return baseRequest.put('/iotnodes/' + iotnodeId + '/status/', {
      body: status
    });
  },
  getUserInfo: () => {
    return baseRequest.get('/users/me');
  },

  getProviders: () => {
    return baseRequest.get('/provider/');
  },
  createProvider: (provider) => {
    return baseRequest.post('/provider', {
      body: provider
    });
  },
  deleteProvider: (id) => {
    return baseRequest.del('/provider/' + id);
  },

  getChannels: () => {
    return baseRequest.get('/channel/');
  },
  createChannel: (channel) => {
    return baseRequest.post('/channel', {
      body: channel
    });
  },
  deleteChannel: (id) => {
    return baseRequest.del('/channel/' + id);
  }
};
