'use strict';

const servicePort = '9999'; // can be changed
const serviceUrl = 'http://theservice.com' + servicePort; // should be changed
const yggioUrl = 'https://api.ygg.io';

module.exports = {
  express: {
    port: servicePort
  },
  yggio: {
    username: 'the_service_yggio_username@theservice.com', // should be changed
    password: 'password', // should be changed
    restApiUrl: yggioUrl + '/api',
    yggioUrl: yggioUrl,
    sessionKey: 'very_secret_session_key811234124124124gce67cdga976ge76aege7d6g', // should be changed
    nodesChannelUrl: serviceUrl + '/nodes',
    provider: {
      name: 'Name of Service', // should be changed
      info: 'Short description of service.', // should be changed
      redirect_uri: serviceUrl + '/', // can be changed
      secret: 'very_secret_secret_12412389571238957sdvbhsd', // should be changed
      default_channel: {
        token: 'myveryverysecretchanneltoken', // should be changed
        nodesChannelUrl: serviceUrl + '/nodes' // can be changed
      }
    }
  }
};
