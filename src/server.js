const config = require('./config');

const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json({
  extended: false
}));

let currentNodeValue;

app.get('/', (req, res) => {
  res.send('The last value reported to Yggio was: ' + currentNodeValue);
});

app.post('/nodes', (req, res) => {
  console.log('Node updated!');
  console.log(JSON.stringify(req.body, undefined, 2));
  currentNodeValue = JSON.stringify(req.body.value, undefined, 2);
  res.sendStatus(200);
});

exports.init = function () {
  app.listen(config.express.port, function () {
    console.log('Listening on port ' + config.express.port);
  });
  return Promise.resolve();
};
