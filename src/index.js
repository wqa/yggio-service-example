const server = require('./server');
const yggio = require('./yggio');

server.init()
.then((result) => {
  console.log('Express HTTP server - ready (' + result + ')');
  return yggio.init();
}, (err) => {
  console.error('Express HTTP server - error during init');
  console.error(err);
})
.then(() => {
  console.log('YGGIO API - ready');
}, (err) => {
  console.error('YGGIO API - error during init');
  console.error(err);
});
