This is a small code sample of how you should use the [Yggio API](ygg.io) to register a service and subscribe to iotnode-events.

It requires [Node.js](https://nodejs.org/).

You can start the application with:
1. `npm install`
2. `npm start`
